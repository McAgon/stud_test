#include <iostream>
#include <vector>
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
typedef _Float64 Pixel;
class calibGenegator
{
    std::vector<cv::Vec2f>  conerPoints;
    public:

    calibGenegator(std::vector<cv::Vec2f> Points): conerPoints(Points)
    {
          std::cout<<"calibGenegator"<<std::endl;
    }

    void get_calib(cv::Mat& rez)
    {
        rez=cv::Mat(cv::Size(4, 3), CV_64FC1);
        rez.forEach<Pixel>([&](Pixel& pixel, const int position[]) -> void {
        pixel = position[0]+position[1];
        });
    }
    
};

int main()
{
    calibGenegator CG({{0,0},{0,1},{0,2},{1,0}});
    cv::Mat A;
    A=cv::Mat(cv::Size(1, 49), CV_64FC1);
    CG.get_calib(A);
    std::cout << " Calibration"<<std::endl<<A << std::endl;
    return 0;
}
